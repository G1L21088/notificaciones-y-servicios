package mx.gilsantaella.notificationsandservices.notification;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import mx.gilsantaella.notificationsandservices.R;
import mx.gilsantaella.notificationsandservices.ResultActivity;

/**
 * Created by Infofast on 11/01/2016.
 */
public class MyNotification {
    private NotificationManager notificationManager;
    private Uri alarmSound;
    private NotificationCompat.Builder notifyBuilder;
    private Bitmap bm;
    private Intent toLaunch;

    public MyNotification(Activity activity, int _NOTIFY_7) {
        notificationManager = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
        alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notifyBuilder = new NotificationCompat.Builder(activity.getApplicationContext());
        notifyBuilder.setSmallIcon(R.mipmap.ic_launcher);
        bm = BitmapFactory.decodeResource(activity.getResources(), R.drawable.ic_info);
        notifyBuilder.setLargeIcon(bm);
        notifyBuilder.setPriority(android.app.Notification.PRIORITY_HIGH);
        notifyBuilder.setTicker("Final");
        notifyBuilder.setWhen(System.currentTimeMillis());
        notifyBuilder.setSound(alarmSound);
        notifyBuilder.setAutoCancel(true);
        notifyBuilder.setVibrate(new long[]{0, 200, 200, 600, 600});
        notifyBuilder.setStyle(new NotificationCompat.BigTextStyle()
                .bigText("This text is a very to\n long text for a full notification\n in the application"));
        toLaunch = new Intent(activity, ResultActivity.class);
        notifyBuilder.setContentIntent(PendingIntent.getActivity(activity, 0, toLaunch, PendingIntent.FLAG_UPDATE_CURRENT));
        notifyBuilder.setContentTitle("Prueba Total");
        notifyBuilder.setContentText("Notificacion Final");
        notificationManager.notify(_NOTIFY_7, notifyBuilder.build());
    }
}
