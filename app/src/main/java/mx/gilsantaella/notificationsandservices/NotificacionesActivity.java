package mx.gilsantaella.notificationsandservices;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.Button;
import android.view.View;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;

import mx.gilsantaella.notificationsandservices.notification.MyNotification;


public class NotificacionesActivity extends AppCompatActivity implements View.OnClickListener{

    private NotificationManager notificationManager;
    private ArrayList<Button> btnNotificaciones;
    private final int _NOTIFY_0 = 0x1010;
    private final int _NOTIFY_1 = 0x1011;
    private final int _NOTIFY_2 = 0x1012;
    private final int _NOTIFY_3 = 0x1013;
    private final int _NOTIFY_4 = 0x1014;
    private final int _NOTIFY_5 = 0x1015;
    private final int _NOTIFY_6 = 0x1016;
    private final int _NOTIFY_7 = 0x1016;
    private final int _WAIT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificaciones);
        setCasting();
    }

    @Override
    public void onClick(View v) {
        Timer timer;
        TimerTask timerTask;
        switch(v.getId()){
            case R.id.btnNotificationPropia:
                timer = new Timer();
                timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        notificationPropia();
                    }
                };
                timer.schedule(timerTask, _WAIT);
                break;
            case R.id.btnNotificationText:
                timer = new Timer();
                timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        triggerNotificationText();
                    }
                };
                timer.schedule(timerTask, _WAIT);
                break;
            case R.id.btnNotificationVibrate:
                timer = new Timer();
                timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        triggerNotificationVibrate();
                    }
                };
                timer.schedule(timerTask, _WAIT);
                break;
            case R.id.btnNotificationLed:
                timer = new Timer();
                timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        triggerNotificationLed();
                    }
                };
                timer.schedule(timerTask, _WAIT);
                break;
            case R.id.btnNotificationNoise:
                timer = new Timer();
                timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        triggerNotificationNoise();
                    }
                };
                timer.schedule(timerTask, _WAIT);
                break;
            case R.id.btnNotificationRemoteView:
                timer = new Timer();
                timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        triggerNotificationRemoteView();
                    }
                };
                timer.schedule(timerTask, _WAIT);
                break;
            case R.id.btnNotificationExpand:
                timer = new Timer();
                timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        triggerNotificationExpand();
                    }
                };
                timer.schedule(timerTask, _WAIT);
                break;
            case R.id.btnNotificationFull:
                timer = new Timer();
                timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        triggerNotificationFull();
                    }
                };
                timer.schedule(timerTask, _WAIT);
                break;
        }
    }

    private void setCasting(){
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        btnNotificaciones = new ArrayList<>();
        btnNotificaciones.add((Button)findViewById(R.id.btnNotificationPropia));
        btnNotificaciones.add((Button)findViewById(R.id.btnNotificationText));
        btnNotificaciones.add((Button)findViewById(R.id.btnNotificationVibrate));
        btnNotificaciones.add((Button)findViewById(R.id.btnNotificationLed));
        btnNotificaciones.add((Button)findViewById(R.id.btnNotificationNoise));
        btnNotificaciones.add((Button)findViewById(R.id.btnNotificationRemoteView));
        btnNotificaciones.add((Button)findViewById(R.id.btnNotificationExpand));
        btnNotificaciones.add((Button)findViewById(R.id.btnNotificationFull));
        setControl();
    }

    private void setControl(){
        for(Button btn : btnNotificaciones){
            btn.setOnClickListener(this);
        }
    }

    private void notificationPropia() {
        Notification notification = new Notification(R.drawable.ic_notification, "¡Nuevo mensaje!", System.currentTimeMillis());
        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.notification_layout);
        contentView.setImageViewResource(R.id.img_notification, R.drawable.ic_info);
        contentView.setTextViewText(R.id.txt_notification, "Ey mundo! Esta es mi notificación personalizada.");
        notification.contentView = contentView;
        Intent notificationIntent = new Intent(this, NotificacionesActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        notification.contentIntent = contentIntent;
        notificationManager.notify(_NOTIFY_0, notification);
    }

    private void triggerNotificationText() {
        NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(getApplicationContext());
        notifyBuilder.setSmallIcon(android.R.drawable.stat_notify_sync);
        notifyBuilder.setTicker("Prueba");
        notifyBuilder.setWhen(System.currentTimeMillis());
        notifyBuilder.setNumber(5);
        notifyBuilder.setAutoCancel(true);
        Intent toLaunch = new Intent(NotificacionesActivity.this, NotificacionesActivity.class);
        notifyBuilder.setContentIntent(PendingIntent.getActivity(NotificacionesActivity.this, 0, toLaunch, PendingIntent.FLAG_UPDATE_CURRENT));
        notifyBuilder.setContentTitle("Prueba Texto");
        notifyBuilder.setContentText("Notificacion de Texto Cancelable");
        notificationManager.notify(_NOTIFY_1, notifyBuilder.build());
    }

    private void triggerNotificationVibrate() {
        NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(getApplicationContext());
        notifyBuilder.setSmallIcon(android.R.drawable.stat_notify_sync);
        notifyBuilder.setTicker("Vibracion");
        notifyBuilder.setWhen(System.currentTimeMillis());
        notifyBuilder.setNumber(5);
        notifyBuilder.setAutoCancel(true);
        notifyBuilder.setVibrate(new long[]{0, 200, 200, 600, 600});
        Intent toLaunch = new Intent(NotificacionesActivity.this, NotificacionesActivity.class);
        notifyBuilder.setContentIntent(PendingIntent.getActivity(NotificacionesActivity.this, 0, toLaunch, PendingIntent.FLAG_UPDATE_CURRENT));
        notifyBuilder.setContentTitle("Prueba Vibracion");
        notifyBuilder.setContentText("Notificacion de Vibracion");
        notificationManager.notify(_NOTIFY_2, notifyBuilder.build());
    }

    private void triggerNotificationLed() {
        NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(getApplicationContext());
        notifyBuilder.setSmallIcon(android.R.drawable.stat_notify_sync);
        notifyBuilder.setTicker("Led");
        notifyBuilder.setWhen(System.currentTimeMillis());
        notifyBuilder.setLights(Color.GREEN, 1000, 1000);
        Intent toLaunch = new Intent(NotificacionesActivity.this, NotificacionesActivity.class);
        notifyBuilder.setContentIntent(PendingIntent.getActivity(NotificacionesActivity.this, 0, toLaunch, PendingIntent.FLAG_UPDATE_CURRENT));
        notifyBuilder.setContentTitle("Prueba Vibracion");
        notifyBuilder.setContentText("Notificacion de Led");
        notificationManager.notify(_NOTIFY_3, notifyBuilder.build());
    }

    private void triggerNotificationNoise() {
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(getApplicationContext());
        notifyBuilder.setSmallIcon(android.R.drawable.stat_notify_sync);
        notifyBuilder.setTicker("Sonido");
        notifyBuilder.setWhen(System.currentTimeMillis());
        notifyBuilder.setSound(alarmSound);
        Intent toLaunch = new Intent(NotificacionesActivity.this, NotificacionesActivity.class);
        notifyBuilder.setContentIntent(PendingIntent.getActivity(NotificacionesActivity.this, 0, toLaunch, PendingIntent.FLAG_UPDATE_CURRENT));
        notifyBuilder.setContentTitle("Prueba Sonido");
        notifyBuilder.setContentText("Notificacion de Sonido");
        notificationManager.notify(_NOTIFY_4, notifyBuilder.build());
    }

    private void triggerNotificationRemoteView() {
        RemoteViews remote = new RemoteViews(getPackageName(), R.layout.remote);

        remote.setTextViewText(R.id.text1, "Big text here!");
        remote.setTextViewText(R.id.text2, "Red text down here!");
        NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(getApplicationContext());
        notifyBuilder.setSmallIcon(android.R.drawable.stat_notify_sync);
        notifyBuilder.setTicker("Sonido");
        notifyBuilder.setWhen(System.currentTimeMillis());
        notifyBuilder.setContent(remote);
        Intent toLaunch = new Intent(NotificacionesActivity.this, NotificacionesActivity.class);
        notifyBuilder.setContentIntent(PendingIntent.getActivity(NotificacionesActivity.this, 0, toLaunch, PendingIntent.FLAG_UPDATE_CURRENT));
        notificationManager.notify(_NOTIFY_5, notifyBuilder.build());
    }

    private void triggerNotificationExpand() {
        NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(getApplicationContext());
        notifyBuilder.setSmallIcon(android.R.drawable.stat_notify_sync);
        notifyBuilder.setTicker("Sonido");
        notifyBuilder.setWhen(System.currentTimeMillis());
        notifyBuilder.setStyle(new NotificationCompat.BigTextStyle()
                .bigText("This text is a very to long text for a full notification in the application"));
        Intent toLaunch = new Intent(NotificacionesActivity.this, NotificacionesActivity.class);
        notifyBuilder.setContentIntent(PendingIntent.getActivity(NotificacionesActivity.this, 0, toLaunch, PendingIntent.FLAG_UPDATE_CURRENT));
        notifyBuilder.setContentTitle("Prueba Expandable");
        notifyBuilder.setContentText("Notificacion Expandible");
        notificationManager.notify(_NOTIFY_6, notifyBuilder.build());
    }

    private void triggerNotificationFull(){
        new MyNotification(this, _NOTIFY_7);
    }
}
