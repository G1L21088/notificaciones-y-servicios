package mx.gilsantaella.notificationsandservices;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import mx.gilsantaella.notificationsandservices.servicio.MyService;

public class ServicioActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicio);

        Button btnSerIniciar = (Button)findViewById(R.id.btnSerIniciar);
        Button btnSerDetener = (Button)findViewById(R.id.btnSerDetener);

        btnSerIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startService(new Intent(getBaseContext(), MyService.class));
            }
        });
        btnSerDetener.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService(new Intent(getBaseContext(), MyService.class));
            }
        });
    }

    @Override
    protected void onPause() {
        startService(new Intent(getBaseContext(), MyService.class));
        super.onPause();
    }
}
