package mx.gilsantaella.notificationsandservices;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import mx.gilsantaella.notificationsandservices.servicio.MyService;

public class MainActivity extends AppCompatActivity {

    Button btnServicio;
    Button btnNotificacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnServicio = (Button)findViewById(R.id.btnServicio);
        btnServicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ServicioActivity.class);
                startActivity(i);
            }
        });
        btnNotificacion = (Button)findViewById(R.id.btnNotificacion);
        btnNotificacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), NotificacionesActivity.class);
                startActivity(i);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        startService(new Intent(getBaseContext(), MyService.class));
    }
}
