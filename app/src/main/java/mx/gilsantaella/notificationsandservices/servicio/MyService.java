package mx.gilsantaella.notificationsandservices.servicio;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import mx.gilsantaella.notificationsandservices.R;
import mx.gilsantaella.notificationsandservices.ResultActivity;

/**
 * Created by Infofast on 11/01/2016.
 */
public class MyService extends Service {

    private Intent toLaunch;
    private NotificationManager notificationManager;
    private NotificationCompat.Builder notifyBuilder;
    private Uri alarmSound;
    private Bitmap bm;
    private final int _NOTIFICATION_ID = 0x01016;
    private final int _WAIT = 5000;
    private final long[] _VIBRATE = {0, 200, 200, 600, 600};
    Timer timer;
    TimerTask timerTask;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notifyBuilder = new NotificationCompat.Builder(getApplicationContext());
        toLaunch = new Intent(getApplicationContext(), ResultActivity.class);
        Toast.makeText(this, "Servicio Creado", Toast.LENGTH_LONG).show();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        initNotification();
        new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                initNotification();
            }

            @Override
            public void onFinish() {
                stopSelf();
            }
        };
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Servicio Terminado", Toast.LENGTH_LONG).show();
        cancelNotification();
    }

    private void initNotification() {
        notifyBuilder.setSmallIcon(R.mipmap.ic_launcher);
        bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_info);
        notifyBuilder.setLargeIcon(bm);
        notifyBuilder.setPriority(android.app.Notification.PRIORITY_HIGH);
        notifyBuilder.setTicker("Final");
        notifyBuilder.setWhen(System.currentTimeMillis());
        notifyBuilder.setSound(alarmSound);
        notifyBuilder.setAutoCancel(true);
        notifyBuilder.setVibrate(_VIBRATE);
        notifyBuilder.setStyle(new NotificationCompat.BigTextStyle()
                .bigText("This text is a very to\n long text for a full notification\n in the application"));
        notifyBuilder.setContentIntent(PendingIntent.getActivity(this, 0, toLaunch, PendingIntent.FLAG_UPDATE_CURRENT));
        notifyBuilder.setContentTitle("Prueba Total");
        notifyBuilder.setContentText("Notificacion Final");
        notificationManager.notify(_NOTIFICATION_ID, notifyBuilder.build());
    }

    private void cancelNotification() {
        notificationManager.cancel(_NOTIFICATION_ID);
    }
}
/*
    private class DoBackgroundTask extends AsyncTask<URL, Integer, Long> {

        @Override
        protected Long doInBackground(URL... params) {
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
            stopSelf();
        }
    }
 */